import Vuex from "vuex";
import { createLocalVue } from "@vue/test-utils";
import { cloneDeep } from "lodash";
import authModule from "@/store/modules/auth";

const fakeToken = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000";

describe("authModule", () => {
  test("isAuthenticated returns false if state.token is empty", () => {
    const state = {
      token: "",
    };
    const getters = authModule.getters;
    expect(getters.isAuthenticated(state)).toBe(false);
  });

  test("isAuthenticated returns true if state.token is set", () => {
    const state = {
      token: fakeToken,
    };
    const getters = authModule.getters;
    expect(getters.isAuthenticated(state)).toBe(true);
  });

  test("it authenticates when SET_TOKEN is commited", () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    const store = new Vuex.Store({
      modules: {
        authModule: cloneDeep(authModule),
      },
    });
    expect(store.getters["authModule/isAuthenticated"]).toBe(false);
    store.commit("authModule/SET_TOKEN", fakeToken);
    expect(store.getters["authModule/isAuthenticated"]).toBe(true);
  });

  test("login commits SET_TOKEN mutation", async () => {
    const commit = jest.fn();
    await authModule.actions.login({ commit });
    expect(commit).toHaveBeenCalledWith("SET_TOKEN", fakeToken);
  });
});

import { shallowMount } from "@vue/test-utils";
import flushPromises from "flush-promises";
import AccountView from "@/views/AccountView.vue";
import i18n from "@/plugins/i18n";

const mockApiResponse = {
  id: 18498492,
  email: "eddy_chirila@yahoo.com",
  signupdate: "29/03/2012",
  type: "Free trial",
  firstname: "Eddy",
  lastname: "Chirila",
  phonenumber: "+40745277422",
  company: "Egoditor GmbH",
  street: "Am Lenkwerk 13",
  city: "Bielefeld",
  zipcode: "invalid on purpose",
  country: 1,
  website: "",
};

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockApiResponse),
  })
);

describe("AccountView.vue", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AccountView, {
      stubs: ["router-link", "font-awesome-icon"],
      i18n,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("renders correctly", () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it("uses a mocked HTTP call when created", async () => {
    await flushPromises();
    expect(wrapper.vm.user).toBe(mockApiResponse);
  });
});

import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import LoginView from "@/views/LoginView.vue";

const localVue = createLocalVue();

localVue.use(Vuex);

describe("LoginView.vue", () => {
  test("calls store action login automatically when component is created", () => {
    const actions = {
      login: jest.fn(),
    };
    const store = new Vuex.Store({
      modules: {
        authModule: {
          namespaced: true,
          actions,
        },
      },
    });
    shallowMount(LoginView, {
      store,
      localVue,
      mocks: {
        $router: {
          push: jest.fn(),
        },
      },
    });
    expect(actions.login).toHaveBeenCalled();
  });
});

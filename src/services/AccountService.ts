/**
 * We are using json-server to fake our REST API. The data is stored within the
 * account.json file
 * See https://www.npmjs.com/package/json-server
 */

import { Account } from "@/types";

export async function getAccount(): Promise<Account> {
  const response = await fetch("http://localhost:3001/account");
  return await response.json();
}

export async function updateAccount(payload: Account): Promise<Account> {
  const response = await fetch("http://localhost:3001/account", {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
  return await response.json();
}

import { required, regex, numeric } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
  ...required,
  message: "This {_field_} field is required.",
});

extend("regex", {
  ...regex,
  message: "The {_field_} format is invalid.",
});

extend("numeric", {
  ...numeric,
  message: "The {_field_} must be a number.",
});

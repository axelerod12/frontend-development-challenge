import { NavigationGuardNext } from "vue-router";
import { Store } from "vuex";
import { State } from "@/store";

export default function auth({
  next,
  store,
}: {
  next: NavigationGuardNext;
  store: Store<State>;
}) {
  if (!store.getters["authModule/isAuthenticated"]) {
    return next({
      name: "LoginView",
    });
  }

  return next();
}

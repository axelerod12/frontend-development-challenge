import { Commit } from "vuex";

export interface AuthState {
  token: string;
  error: string | null;
}

const state = (): AuthState => ({
  token: "",
  error: null,
});

const getters = {
  isAuthenticated(state: AuthState) {
    return !!state.token;
  },
};

const mutations = {
  SET_TOKEN(state: AuthState, payload: string) {
    state.token = payload;
  },
};

const actions = {
  login({ commit }: { commit: Commit }) {
    // this is an example application so we are going to simulate an API request that resolves
    // with a token
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        commit("SET_TOKEN", "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000");
        resolve();
      }, 1000);
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

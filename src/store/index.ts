import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import authModule, { AuthState } from "./modules/auth";

Vue.use(Vuex);

export interface State {
  auth: AuthState;
}

export default new Vuex.Store<State>({
  modules: {
    authModule,
  },
  plugins: [
    createPersistedState({
      paths: ["authModule"],
    }),
  ],
});

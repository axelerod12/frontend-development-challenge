export interface Account {
  id: number;
  email?: string;
  signupdate: string;
  type: "Free trial" | "Professional";
  firstname?: string;
  lastname?: string;
  telephone?: string;
  company?: string;
  street?: string;
  city?: string;
  zip?: number;
  country?: number;
  website?: string;
}

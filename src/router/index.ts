import Vue from "vue";
import VueRouter, { NavigationGuardNext, Route } from "vue-router";
import authMiddleware from "@/middlewares/auth";
import routes from "./routes";
import store from "@/store";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to: Route, from: Route, next: NavigationGuardNext) => {
  if (to.meta?.requiresAuth && !store.getters["authModule/isAuthenticated"]) {
    const context = {
      to,
      from,
      next,
      store,
    };

    authMiddleware({ ...context });
  } else {
    // make sure you always call next()
    next();
  }
});

export default router;

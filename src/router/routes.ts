import { RouteConfig } from "vue-router";
import LoginView from "@/views/LoginView.vue";

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "LoginView",
    component: LoginView,
    meta: {
      layout: "EmptyLayout",
    },
  },
  {
    path: "/account",
    name: "AccountView",
    meta: {
      title: "Account",
      requiresAuth: true,
      layout: "DashboardLayout",
    },
    component: () =>
      import(/* webpackChunkName: "AccountView" */ "../views/AccountView.vue"),
  },
  {
    path: "*",
    name: "NotFoundView",
    component: () =>
      import(/* webpackChunkName: "NotFoundView" */ "@/views/NotFoundView.vue"),
    meta: {
      layout: "EmptyLayout",
    },
  },
];

export default routes;

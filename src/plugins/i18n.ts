import Vue from "vue";
import VueI18n, { I18nOptions } from "vue-i18n";
import locales from "@/locales";

Vue.use(VueI18n);

const opts: I18nOptions = {
  locale: "en",
  fallbackLocale: "en",
  messages: locales,
};

export default new VueI18n(opts);

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "@/plugins/i18n";
import "@/assets/css/main.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(far);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component(
  "DashboardLayout",
  () =>
    import(
      /* webpackChunkName: "DashboardLayout" */ "./layouts/DashboardLayout.vue"
    )
);
Vue.component(
  "EmptyLayout",
  () =>
    import(/* webpackChunkName: "EmptyLayout" */ "./layouts/EmptyLayout.vue")
);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
